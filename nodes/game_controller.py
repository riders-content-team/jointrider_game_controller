#!/usr/bin/env python

import rospy
from std_msgs.msg import String
import json
from gazebo_msgs.srv import GetModelState, GetLinkState
from std_srvs.srv import Trigger, TriggerResponse, Empty
from jointrider_arena.srv import SpawnButtonMarker
import time
import math
import os
import requests

class GameController:

    def __init__(self, robot_name):

        self.robot_name = robot_name

        self.robot_status = "Paused"

        self.buttons = {
            "A" : [math.sqrt(8), 0, False, 142],
            "B" : [0, math.sqrt(8), False, 143],
            "C" : [0, 2, False, 144],
            "D" : [0, -2, False, 145],
            "E" : [-1.5, 0, False, 146],
            "F" : [1, 1, False, 147]
        }

        self.metrics = {
            'Robot Status' : self.robot_status,
        }

        rospy.init_node("game_controller")

        self.metric_pub = rospy.Publisher('simulation_metrics', String, queue_size=10)
        metric_msg_rate = 10
        rate = rospy.Rate(metric_msg_rate)

        s1 = rospy.Service('/robot_handler', Trigger, self.handle_robot)
        s2 = rospy.Service('/robot_collided', Trigger, self.robot_collide_handler)

        try:
            rospy.wait_for_service("/gazebo/get_model_state", 1.0)
            self.model_state = rospy.ServiceProxy("/gazebo/get_model_state", GetModelState)
            
            rospy.wait_for_service("/gazebo/get_model_state", 1.0)
            self.link_state = rospy.ServiceProxy("/gazebo/get_link_state", GetLinkState)

            rospy.wait_for_service("/spawn_button_marker", 1.0)
            self.spawn_button_marker = rospy.ServiceProxy("/spawn_button_marker", SpawnButtonMarker)

            resp = self.model_state("jointrider", "")
            if not resp.success == True:
                self.robot_status = "No Robot"

        except (rospy.ServiceException, rospy.ROSException), e:
            self.robot_status = "No Robot"
            rospy.logerr("Service call failed: %s" % (e,))

        self.now = rospy.get_time()
        self.start_time = self.now
        
        while ~rospy.is_shutdown():
            self.check_robot_tip()
            self.publish_metrics()
            rate.sleep()
            if self.robot_status == "Stop":
                break


    def handle_robot(self,req):
        if self.robot_status == "Paused":
            self.robot_status = "Running"
            self.start_time = rospy.get_time()

            return TriggerResponse(
                success = True,
                message = self.robot_status
            )

        elif self.robot_status == "Running":
            return TriggerResponse(
                success = True,
                message = self.robot_status
            )

        elif self.robot_status == "Stop":
            return TriggerResponse(
                success = True,
                message = self.robot_status
            )

        elif self.robot_status == "No Robot":
            return TriggerResponse(
                success = True,
                message = self.robot_status
            )

        elif self.robot_status == "Robot Collided With Ground":
            return TriggerResponse(
                success = True,
                message = "Stop"
            )

    def robot_collide_handler(self, req):
        self.robot_status = "Robot Collided With Ground"

        return TriggerResponse(
            success = True,
            message = ""
        )   

    def publish_metrics(self):
        self.metrics['Robot Status'] = str(self.robot_status)
        self.metrics['Time'] = str(self.now - self.start_time)

        for key in self.buttons:
            button_pressed_string = str(self.buttons[key][2]).capitalize()
            self.metrics[key] = button_pressed_string

        self.metric_pub.publish(json.dumps(self.metrics, sort_keys=True))


    def check_robot_tip(self):
        resp = self.link_state("end_point", "")
        tip_position = resp.link_state.pose.position
    
        for key in self.buttons:
            if ((not self.buttons[key][2]) and abs(tip_position.x - self.buttons[key][0]) < 0.1 and abs(tip_position.y - self.buttons[key][1]) < 0.1 and tip_position.z < 0.2):
                self.buttons[key][2] = True
                self.spawn_button_marker(key, self.buttons[key][0], self.buttons[key][1])
                self.achieve(self.buttons[key][3])
            
            if (not self.buttons[key][2] and self.robot_status == "Running"):
                self.now = rospy.get_time()

    def achieve(self, achievement_id):
        # get parameters from environment
        host = os.environ.get('RIDERS_HOST', None)
        project_id = os.environ.get('RIDERS_PROJECT_ID', None)
        token = os.environ.get('RIDERS_AUTH_TOKEN', None)

        # generate request url
        achievement_url = '%s/api/v1/project/%s/achievements/%s/achieve/' % (host, project_id, achievement_id)

        # send request
        rqst = requests.post(achievement_url, headers={
            'Authorization': 'Token %s' % token,
            'Content-Type': 'application/json',
        })

if __name__ == '__main__':
    # Name of robot
    controller = GameController("jointrider")
